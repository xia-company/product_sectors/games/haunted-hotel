import random


class Ghost:
    def __init__(self):
        self.x = random.randint(0, 10)
        self.y = random.randint(0, 10)

    def move(self):
        # Update ghost position
        pass

    def collide(self, player):
        # Check collision between ghost and player
        return False