```mermaid
classDiagram
    class Game{
        -Player player
        -List[Ghost] ghosts
        -List[Room] rooms
        -UserInterface ui
        +__init__(self)
        +start(self)
        +update(self)
        +render(self)
    }

    class Player{
        -int x
        -int y
        +__init__(self)
        +move(self, direction: str)
        +collide(self, ghost: Ghost) -> bool
    }

    class Ghost{
        -int x
        -int y
        +__init__(self, x: int, y: int)
        +move(self)
        +collide(self, player: Player) -> bool
    }

    class Room{
        -int number
        -str name
        +__init__(self, number: int, name: str)
        +get_number(self) -> int
        +get_name(self) -> str
    }

    class UserInterface{
        +__init__(self)
        +show_menu(self)
        +show_game_over(self)
    }

    class GameMenu{
        +__init__(self)
        +start_game(self)
        +exit_game(self)
    }

    class GameOverMenu{
        +__init__(self, score: int)
        +show_score(self)
        +play_again(self)
        +exit_game(self)
    }

    class GameManager{
      +__init__(self)
      +start_game(self)
      +update_game(self)
      +render_game(self)
    }

    class InputManager{
        +__init__(self)
        +get_input(self) -> str
    }
    
    Game "1" -- "1" Player: has
    Game "1" -- "n" Ghost: has
    Game "1" -- "n" Room: has
    Game "1" -- "1" UserInterface: has
    UserInterface "1" -- "1" GameMenu: has
    UserInterface "1" -- "1" GameOverMenu: has
    GameMenu "1" --> "1" GameManager: use
    GameOverMenu "1" --> "1" GameManager: use
    GameManager "1" --> "1" Game: manages
    GameManager "1" --> "1" InputManager: use
    InputManager "1" --> "1" UserInterface: use
```