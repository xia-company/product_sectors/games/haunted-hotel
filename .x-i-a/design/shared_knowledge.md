```python
[
    "game.py contains the Game class, which manages the game logic, such as starting the game, updating the game state, and rendering the game on the user interface.",
    "player.py contains the Player class, which represents the player character and handles player movement and collision detection with the ghosts.",
    "ghost.py contains the Ghost class, which represents the enemy ghosts and handles ghost movement and collision detection with the player.",
    "room.py contains the Room class, which represents the different rooms in the haunted hotel.",
    "user_interface.py contains the UserInterface class, which handles user input and displaying the game menus and game over screen.",
]
```