```mermaid
sequenceDiagram
    participant M as Main
    participant GM as GameManager
    participant UI as UserInterface
    participant IM as InputManager
    participant G as Game
    participant P as Player
    participant G1 as Ghost
    participant G2 as Ghost
    participant R1 as Room
    ...
    M->>GM: start_game
    activate GM
    GM->>G: __init__
    activate G
    G->>P: __init__
    activate P
    G->>G1: __init__
    activate G1
    G->>G2: __init__
    activate G2
    G->>R1: __init__
    activate R1
    G->>UI: __init__
    activate UI
    GM-->>UI: show_menu
    activate UI
    UI-->>IM: get_input
    activate IM
    IM-->>UI: start_game
    activate UI
    UI-->>GM: start_game
    deactivate UI
    GM-->>G: start
    activate G
    loop Game Loop
        G->>P: move
        activate P
        P-->>G1: collide
        activate G1
        G1-->>P: True/False
        deactivate G1
        P-->>G2: collide
        activate G2
        G2-->>P: True/False
        deactivate G2
        G-->>UI: show_game_over
        activate UI
        UI-->>IM: get_input
        activate IM
        IM-->>UI: play_again
        activate UI
        UI-->>GM: start_game
        deactivate UI
        GM-->>G: start
        deactivate G
    end
```