class UserInterface:
    def __init__(self):
        self.game_menu = GameMenu()
        self.game_over_menu = GameOverMenu()

    def show_menu(self):
        self.game_menu.start_game()

    def show_game_over(self):
        self.game_over_menu.show_score()
        if self.game_over_menu.play_again():
            return True
        else:
            return False

class GameMenu:
    def __init__(self):
        pass

    def start_game(self):
        # Display game start menu
        pass

class GameOverMenu:
    def __init__(self):
        self.score = 0

    def show_score(self):
        # Display game over screen with score
        pass

    def play_again(self):
        # Ask user if they want to play again
        return False