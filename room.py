class Room:
    def __init__(self, number, name):
        self.number = number
        self.name = name

    def get_number(self):
        return self.number

    def get_name(self):
        return self.name