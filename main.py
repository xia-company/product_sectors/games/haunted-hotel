import pygame
from haunted_hotel.game_manager import GameManager

def main():
    pygame.init()
    game_manager = GameManager()
    game_manager.start_game()

if __name__ == "__main__":
    main()