from haunted_hotel.player import Player
from haunted_hotel.ghost import Ghost
from haunted_hotel.room import Room
from haunted_hotel.user_interface import UserInterface


class Game:
    def __init__(self):
        self.player = Player()
        self.ghosts = [Ghost(), Ghost()]
        self.rooms = [Room(1, "Room 1"), Room(2, "Room 2"), Room(3, "Room 3")]
        self.ui = UserInterface()

    def start(self):
        self.ui.show_menu()

    def update(self):
        self.player.move()
        for ghost in self.ghosts:
            if self.player.collide(ghost):
                self.ui.show_game_over()
                if self.ui.play_again():
                    self.start()
                else:
                    return

    def render(self):
        # Render game objects on screen
        pass